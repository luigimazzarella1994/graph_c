#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INFINITO 9999


typedef struct citta {
	
	char cityName[100];
	int peso;
	int prezzo;
	int gettonata;
	struct citta *next;
	
}citta;

typedef struct nodoCitta {
	
	char name[100];
	citta* adiacente;
	
} nodoCitta;

typedef struct dijk {
	
	char nome[100];
	int peso;
	char pred[100];
	
}dijk;

typedef struct cittaVisitate {
	
	char nome[100];
	int visitato;
	
}visitate;

citta *addPercorso(nodoCitta nodo, citta *top, int peso){
	
	citta *tmp = malloc(sizeof(citta));
	strcpy(tmp->cityName, nodo.name);
	tmp->peso = peso;
	tmp->next = NULL;
	citta *tmp2 = top;
	
	if (top==NULL){
		top = tmp;
	}else{
		tmp->next = top;
		top= tmp;
	}
	return top;
}

int tuttiVisitati(visitate *cittaVisitate){
	 int i = 0, count = 0;
	 
	 for (i = 0; i<20;i++ ){
	 	
	 	if (cittaVisitate[i].visitato == 1) count++;
	 }
	 
	 if(count == i) {
	 	return 1;
	 }
	 return 0;
	
}

citta *dijkstraSuCosto(nodoCitta *G,char *partenza, citta *percorso, char *arrivo){

	dijk ArrayDijk2[20];
	visitate cittaVisitate2[20];
	int i = 0;
	int min = INFINITO;	
	int km = 0;
	
	

	for (i = 0;i<20; i++) {

		strcpy(	ArrayDijk2[i].nome  , G[i].name);
		ArrayDijk2[i].peso =  INFINITO;
		strcpy(ArrayDijk2[i].pred,"NESSUNO");
		strcpy(	cittaVisitate2[i].nome  , G[i].name);
		cittaVisitate2[i].visitato = 0;
		
	}
	

	
	int indice = 0;
	
	for(i = 0;i<20; i++){
		
		
		if(strcmp(G[i].name,partenza)== 0){
			
			indice = i;
			break;
		}
		
	}
	
	
	
	ArrayDijk2[indice].peso = 0;
	int sonoArrivato = 0;
	
	while (!tuttiVisitati(cittaVisitate2)) {
		
			
		citta *adj = G[indice].adiacente;
	
		int giaVisitato = 0;
		while(adj != NULL ) {
			giaVisitato = 0;
			for (i=0;i<20;i++) {
				if (strcmp(adj->cityName, cittaVisitate2[i].nome) == 0 ){
					
					if (cittaVisitate2[i].visitato == 1 ){
						
						giaVisitato=1;
					}
					break;
				}
				
			}
			if (!giaVisitato){
				
				int y = 0;
				for(y= 0; y<20;y++ ) {
				
					if(strcmp(ArrayDijk2[y].nome,adj->cityName) == 0) {
					
							break;
						}
				}
				int j= 0;
				int pesoPred = 0;
				for(j = 0; j<20;j++){
				
					if(strcmp(ArrayDijk2[j].nome,G[indice].name) == 0)  {
						pesoPred = ArrayDijk2[j].peso;
						break;
					}
				}
				
				
				if(ArrayDijk2[y].peso == INFINITO && min == INFINITO) {
					ArrayDijk2[y].peso = 0 + adj->prezzo;
					strcpy(ArrayDijk2[y].pred,G[indice].name);
				
				}
				else if(ArrayDijk2[y].peso >  pesoPred + adj->prezzo){
					ArrayDijk2[y].peso = pesoPred + adj->prezzo;
					strcpy(ArrayDijk2[y].pred,G[indice].name);
				
				}
				
			
			}
			
		
			adj =  adj->next;
			
		}
		
		
		cittaVisitate2[indice].visitato = 1;
		
	
		
		char citta[100];
		min = INFINITO;
		for (i=0;i<20;i++) {
			
			if(cittaVisitate2[i].visitato != 1){
					
				if( ArrayDijk2[i].peso <= min ) {
						strcpy(citta,ArrayDijk2[i].nome);
						min = ArrayDijk2[i].peso;
					
					}
				}		
		}
		
	
		
		
		
		
		for (i = 0; i<20; i++){
			
			if(strcmp(G[i].name,citta) == 0) {
				indice = i;
				break;	
			}
		}
		
	}
	

	
	char *primoElemento;
	memset(primoElemento,0,100);
	strcpy(primoElemento,arrivo);
	
	for (i= 0;i<20;i++){
		if(strcmp(G[i].name,primoElemento) == 0) break;
	}
	
	km=ArrayDijk2[i].peso;
	percorso = addPercorso(G[i],percorso,ArrayDijk2[i].peso);
	for(i=0;i<20;i++) {
		int y = 0;
		
		if(strcmp(primoElemento, ArrayDijk2[i].nome) == 0){
			for (y =0;y<20;y++) {
				if(strcmp(G[y].name,ArrayDijk2[i].pred) == 0){
					
					break;
				}
			}
		
			strcpy(primoElemento,G[y].name );
		
			percorso = addPercorso(G[y],percorso,ArrayDijk2[i].peso);
		
			if(strcmp(ArrayDijk2[y].pred,"NESSUNO") == 0) {
				
				break;
			}
			
			
			i= -1;
		}
	
			
	}
	
	if(km== INFINITO || strcmp(percorso->cityName,partenza) != 0){
		printf("\nNON HO TROVATO NESSUN PERCORSO!\n");
		return NULL;
	}
	printf("\nCosta %d euro",km);

	return percorso;
	
	
}

citta *dijkstra(nodoCitta *G,char *partenza, citta *percorso, char *arrivo){

	dijk ArrayDijk[20];
	visitate cittaVisitate[20];
	int i = 0;
	int min = INFINITO;	
	int km = 0;

	for (i = 0;i<20; i++) {

		strcpy(	ArrayDijk[i].nome  , G[i].name);
		ArrayDijk[i].peso =  INFINITO;
		strcpy(ArrayDijk[i].pred,"NESSUNO");
		strcpy(	cittaVisitate[i].nome  , G[i].name);
		cittaVisitate[i].visitato = 0;
		
	}
	
	
	
	
	int indice = 0;
	
	for(i = 0;i<20; i++){
		
		
		if(strcmp(G[i].name,partenza)== 0){
			
			indice = i;
			break;
		}
		
	}
	
	ArrayDijk[indice].peso = 0;
	int sonoArrivato = 0;
	
	while (!tuttiVisitati(cittaVisitate) ) {
		
			
		citta *adj = G[indice].adiacente;
	
		int giaVisitato = 0;
		while(adj != NULL ) {
			giaVisitato = 0;
			
			for (i=0;i<20;i++) {
				if (strcmp(adj->cityName, cittaVisitate[i].nome) == 0 ){
					
					if (cittaVisitate[i].visitato == 1 ){
						
						giaVisitato=1;
					}
					break;
				}
				
			}
			if (!giaVisitato){
				
				int y = 0;
				for(y= 0; y<20;y++ ) {
				
					if(strcmp(ArrayDijk[y].nome,adj->cityName) == 0) {
					
							break;
						}
				}
				int j= 0;
				int pesoPred = 0;
				for(j = 0; j<20;j++){
				
					if(strcmp(ArrayDijk[j].nome,G[indice].name) == 0)  {
						pesoPred = ArrayDijk[j].peso;
						break;
					}
				}
			
				if(ArrayDijk[y].peso == INFINITO && min == INFINITO) {
					ArrayDijk[y].peso = 0 + adj->peso;
					strcpy(ArrayDijk[y].pred,G[indice].name);
				
				}
				else if(ArrayDijk[y].peso >  pesoPred + adj->peso){
					ArrayDijk[y].peso = pesoPred + adj->peso;
					strcpy(ArrayDijk[y].pred,G[indice].name);
				
				}
				
			
			}
			
		
			
			adj =  adj->next;
			
		}
		
		cittaVisitate[indice].visitato = 1;
	
	
		char citta[100];
		min = INFINITO;
			for (i=0;i<20;i++) {
			
			if(cittaVisitate[i].visitato != 1){
					
				if( ArrayDijk[i].peso <= min ) {
						strcpy(citta,ArrayDijk[i].nome);
						min = ArrayDijk[i].peso;
					
					}
				}		
		}
		
		
		
		
		
		for (i = 0; i<20; i++){
			
			if(strcmp(G[i].name,citta) == 0) {
				indice = i;
				break;	
			}
		}
		
		
	}
	

	
	char primoElemento[100];
	memset(primoElemento,0,100);
	strcpy(primoElemento,arrivo);
	
	for (i= 0;i<20;i++){
		if(strcmp(G[i].name,primoElemento) == 0) break;
	}
	
	km=ArrayDijk[i].peso;
	percorso = addPercorso(G[i],percorso,ArrayDijk[i].peso);
	for(i=0;i<20;i++) {
		int y = 0;
		
		if(strcmp(primoElemento, ArrayDijk[i].nome) == 0){
			for (y =0;y<20;y++) {
				if(strcmp(G[y].name,ArrayDijk[i].pred) == 0){
					
					break;
				}
			}
		
			strcpy(primoElemento,G[y].name );
		
			percorso = addPercorso(G[y],percorso,ArrayDijk[i].peso);
		
			if(strcmp(ArrayDijk[y].pred,"NESSUNO") == 0) {
				
				break;
			}
			
			
			i= -1;
		}
	
			
	}
	
	if(km== INFINITO || strcmp(percorso->cityName,partenza) != 0){
		printf("\nNON HO TROVATO NESSUN PERCORSO!\n");
		return NULL;
	}
	printf("\nHo Percorso %dKm",km);
	
	int j= 0;
	return percorso;
	
	
}

char lowCase(char *v){
	int i;
     for (i=0; i<strlen(v); i++){
     	if ( (v[i]>='A') && (v[i]<='Z') )
				v[i]+=32;
	 }
      
	  
	  return v[0];    

}

int checkNodo(nodoCitta *G,char citta[] ){
	int i = 0;
	for(i=0;i<20;i++){
		if(strcmp(G[i].name, citta) == 0){
			return 1;
		}
	}
	
	printf("\nCitt� non trovata, riprova\n");
	return 0;
	
}

int checkUser(FILE *pUtenti,char nomeUtente[],char passUtente[]) {
	
	char nome[100];
	char pass[100];
	pUtenti =fopen("Login.txt","r");
	
	if (pUtenti == NULL) {
		printf("\nErrore di Lettura");
		exit(1);
	}
	while(!feof(pUtenti)){
		
		fscanf(pUtenti,"%s",nome);
		fscanf(pUtenti,"%s",pass);
		if(strcmp(nome,nomeUtente) == 0 && strcmp(pass,passUtente) == 0) {
			printf("\nBenvenuto %s!\n",nome);
			fclose(pUtenti);
			return 1;
		}
	}
	fclose(pUtenti);
	return 0;
}

int verificaEsistenza(FILE *pUtenti,char username[]){
	
	char userInFile[100];
	char appoggio[100];
	pUtenti =fopen("Login.txt","r");
	
	if (pUtenti == NULL) {
		printf("\nErrore di Lettura");
		exit(1);
	}
	while(!feof(pUtenti)){
		
		fscanf(pUtenti,"%s %s",userInFile,appoggio);
		if(strcmp(userInFile,username) == 0){
			printf("\nNome utente non disponibile, riprova!\n");
			fclose(pUtenti);
			return 0;
		} 
	}
	fclose(pUtenti);
	return 1;
	
}

void registra(FILE *pUtenti){
	
	
	char username[100];
	char password[100];
	do{
		
		printf("\nInserisci un nome utente: ");
		scanf("%s",username);

	}while(!verificaEsistenza(pUtenti,&username[0]));
	printf("\nInserisci una password ");
	scanf("%s",password);
	
	pUtenti =fopen("Login.txt","a");
	
	if (pUtenti == NULL) {
		printf("\nErrore di Lettura");
		exit(1);
	}
	fprintf(pUtenti,"\n%s %s",username,password);
	fclose(pUtenti);
	
}

void prenota(FILE *pPrenota,citta* percorso, char username []) {
	
	pPrenota = fopen("Prenotazioni.txt","a");
	
	fprintf(pPrenota,"\n%s ",username);
	citta *tmp=percorso;
	while(tmp != NULL) { 
		if(tmp->next != NULL) {
				
			fprintf(pPrenota,"%s -> ", tmp->cityName);
			tmp = tmp ->next;
		}else {
			fprintf(pPrenota,"%s", tmp->cityName);
			tmp = tmp ->next;
		}
		
	}	
	
	fclose(pPrenota);

}

void aggiuntaPercorso(FILE *pPrenota,char username[],nodoCitta *list){

	int scelta2 = -1;
	do{
		
		char partenza[100];
		char arrivo[100];
	
	
		do{
		
			printf("\nInserisci la citt� di partenza: ");
			scanf("%s",partenza);
			partenza[0] = lowCase(&partenza[0]);
		
		}while(!checkNodo(&list[0],&partenza[0]));	
	
		do{
		
			printf("\nInserisci la citt� di arrivo: ");
			scanf("%s",arrivo);
			arrivo[0] = lowCase(&arrivo[0]);
		
		}while(!checkNodo(&list[0],&arrivo[0]));

	
		
		
		
		citta *percorso = NULL;
		percorso = dijkstra(&list[0],partenza, percorso,arrivo);
		citta *tmp = percorso;
		if(tmp != NULL){
			printf("\n1-Tratta pi� veloce\n"); 
			while(tmp != NULL) {
				if(tmp->next != NULL) {
				
					printf("%s -> ", tmp->cityName);
					tmp = tmp ->next;
				}else {
					printf("%s", tmp->cityName);
					tmp = tmp ->next;
				}
		
			}
		}
	

	
		citta *percorso2 = NULL;
		percorso2 = dijkstraSuCosto(&list[0],partenza, percorso2,arrivo);
		citta *tmp2 = percorso2;
		if(tmp2 != NULL){
			printf("\n2-Tratta con costo minore\n");
			while(tmp2 != NULL) {
				if(tmp2->next != NULL) {
			
					printf("%s-> ", tmp2->cityName);
					tmp2 = tmp2 ->next;
				}else {
					printf("%s", tmp2->cityName);
					tmp2 = tmp2 ->next;
				}
		
			}
		}
	
		printf("\n3- Cambiare citt�\n0- Uscire ");
		scanf("%d",&scelta2);
		switch (scelta2){
			case 1: 
				prenota(pPrenota, percorso,&username[0]);
				break;
			case 2:
				prenota(pPrenota, percorso2,&username[0]);
				break;
			case 3: break;
			case 0: break;
			default: 
				printf("\nAzione non riconosciuta\nRIPROVA");
				break;
		}
	}while(scelta2 != 0 && scelta2 != 1 && scelta2 != 2);
	
}

void visualizzaPrenotazioni(FILE *pPrenota, char username[]) {
	
	char appoggio[100];
	char citta[500];
	pPrenota = fopen("Prenotazioni.txt","r");
	int count  = 0;
	while(!feof(pPrenota)){
		
		fscanf(pPrenota,"%s",appoggio);
		if(strcmp(appoggio,username) == 0) {
			count++;
			printf("\n%d- ",count);
			fgets(citta,500,pPrenota);
			printf("%s",citta );
			
		}
		
	}
	
	fclose(pPrenota);
	
}

int main() {
	
	nodoCitta *list = NULL;
	FILE *pCitta = NULL;
	
	int scelta=0;
	FILE *pUtenti = NULL;
	int check = 0;
	
	FILE *pPrenota = NULL;
	char username[100];
	
	do{
		
		char password[100];
		
		printf("-------------Menu------------");
		printf("\n1-Login\n2-Registrati\n0-Uscita ");
		scanf("%d",&scelta);
		switch (scelta) {
			case 0:
				printf("Ciao!"); 
				exit(0);
			case 1: 
				printf("\nInserisci username: ");
				scanf("%s",username);
				printf("\nInserisci password: ");
				scanf("%s",password);
				check = checkUser(pUtenti,&username[0],&password[0]);
				if(!check) {
					printf("\nNessun utente trovato!\nRIPROVA");
				}
				break;
			case 2:
				registra(pUtenti); 
				break;
			default:
				printf("\nAzione non riconosciuta!\n"); 
				break;
		}
	}while(!check);
	


	
	
	list = (nodoCitta*)malloc(20*sizeof(nodoCitta));
	
	pCitta = fopen("Destinazioni.txt","r");
	
	if (pCitta == NULL) {
		
		printf("\nErrore di Lettura");
		exit(1);
	}
	
	int count = 0;
	
	while(!feof(pCitta)) {
		char citta[100];
		char peso[100];
		int pesoInt=0;
		char buff[300];
		int i = 0;
		char prezzo[100];
		char ambita[100];
		int prezzoInt= 0;
		int ambitaInt = 0;
		
		
		//fgets prende in input da file un'intera riga fino al carattere '\n'
		fgets(buff,300,pCitta);	
		//divisione della riga
		if (buff[0] != '\n'){
			
			while(buff[i] != ',') {
			
				list[count].name[i] = buff[i];
				list[count].name[i+1] ='\0';
				i++;
			}
			
			
			while ( buff[i] != '.') {
				
				
				i++;
				while(buff[i] != '-'  && buff[i] != '.') {
					
					
					
					int index = 0;
					memset(prezzo,0,100);
					memset(ambita,0,100);
						
					while(buff[i] != ','){	 
						citta[index] = buff[i];
						citta[index+1] = '\0';
						index++;
						i++;
					}
					i++;
					index = 0;
				
					while(buff[i] != ','){
						peso[index]=buff[i];
						index++;
						i++;
					}
					
					peso[index+1] = '\0';
					pesoInt=atoi(&peso[0]);
					
					i++;
					index = 0;
					while(buff[i] != ','){
						prezzo[index]=buff[i];
						index++;
						i++;
					}
					prezzo[index+1] = '\0';
					prezzoInt = atoi(&prezzo[0]);
					i++;
					index = 0;
					while(buff[i] != '.' && buff[i] != '-'){
						ambita[index]=buff[i];
						index++;
						i++;
					}
					
					ambita[index+1] = '\0';
					ambitaInt= atoi(&ambita[0]);
						
					if (list[count].adiacente == NULL) {
						list[count].adiacente =(struct citta*) malloc(sizeof(struct citta));
						strcpy(list[count].adiacente->cityName,citta );
						list[count].adiacente->peso = pesoInt;
						list[count].adiacente->prezzo = prezzoInt;
						list[count].adiacente->gettonata = ambitaInt;
						list[count].adiacente->next = NULL;
							
					}else{
							
							
						struct citta *nuovoNodo = (struct citta*) malloc(sizeof(struct citta));
						strcpy(nuovoNodo ->cityName,citta );
						nuovoNodo->peso = pesoInt;
						nuovoNodo->prezzo = prezzoInt;
						nuovoNodo->gettonata = ambitaInt;
						nuovoNodo->next = list[count].adiacente;
						list[count].adiacente = nuovoNodo;
							
					}
						
					
						memset(citta,0,strlen(citta));
		
				}
				
				
			 
			}
				i++;
				memset(buff,0,300);
		}
		
		
 		
		count++;
		
	}
	
	fclose(pCitta);
	
	int i = 0;
	

		do  {
		printf("\n------------Menu----------------\n");
		printf("1- Visualizza prenotazioni\n2- Aggiungi prenotazioni\n0- Per uscire ");
		scanf("%d",&scelta);
	
		switch (scelta) {
		case 0: 
			printf("\nCiao!\n");
			break;
			
		case 1: 
			//visualizza prenotazioni
			visualizzaPrenotazioni(pPrenota,&username[0]);
			break;
		case 2:
			//Aggiungi un nuovo percorso
			aggiuntaPercorso(pPrenota,&username[0],list);
			break;
		default: 
			printf("\nAzione non riconosciuta\nRIPROVA");
			break;
			
	}
	
	}while(scelta != 0);
	
	
	return 0;
}
